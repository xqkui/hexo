---
title: 关于
date: 2021-08-16 08:04:34
type: "about"
---

<div align=center>这个地方用来记录我的一些胡思乱想，</div>
<div align=center>也会有我所喜爱的一些东西。</div>
<div align=center>我的博客：<a href="https://blog.xqkui.tk">这里</a></div>